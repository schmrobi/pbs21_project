Setting up environment:
From the project root, run:

    pip install -r requirements.txt

to set up the virtual environment needed to run the simulation.

Running the simulation:
1. See the available command layout:
   From the project root, run:

        python3 main.py --help

   to see the available run options.

2. See the available game configurations:
   From the project root, run:

        python3 main.py --list

   This will print out all the available configuration names.

3. Run the simulation using your selected configuration:
   You can use the previous option to print out and copy one of the available configuration names.
   Then, from the project root, run:

        python3 main.py --conf <chosen_conf>
