import argparse
from pathlib import Path
from src.Parser import Parser
from src.Game import Game
from typing import List, Optional


PROJECT_ROOT_PATH = Path(__file__).parent
GAME_SETUPS_DIR_PATH = PROJECT_ROOT_PATH.joinpath(Path(f"Configurations/GameSetups")).resolve()


def get_available_game_confs() -> List[str]:
    if not GAME_SETUPS_DIR_PATH.exists():
        raise FileNotFoundError(f"Game configuration directory {GAME_SETUPS_DIR_PATH} does not exist!")
    if not GAME_SETUPS_DIR_PATH.is_dir():
        raise NotADirectoryError(f"Game configuration directory path {GAME_SETUPS_DIR_PATH} is not a valid directory!")
    confs = list()
    for f in GAME_SETUPS_DIR_PATH.iterdir():
        if f.suffix == ".json":
            confs.append(f.stem)
    return confs


def parse_arguments() -> Optional[Path]:
    parser = argparse.ArgumentParser(description="Runs the game simulation.")
    parser.add_argument("--conf", type=str, help="the name of the game's configuration.")
    parser.add_argument("--list", action="store_true", help="list the names of available game configuration.")
    args = parser.parse_args()

    if args.list:
        game_confs = get_available_game_confs()
        print("The available game configurations are:\n\t" + "\n\t".join(game_confs))
        return None

    conf_relative_path = Path(f"{args.conf}.json")
    conf_path = GAME_SETUPS_DIR_PATH.joinpath(conf_relative_path).resolve()
    if not conf_path.exists():
        game_confs = get_available_game_confs()
        print(
            f"Game configuration {conf_path} could not be found.\nThe available configurations are:\n\t" +
            "\n\t".join(game_confs)
        )
        return None

    return conf_path


def main(game_conf_path: Path):

    parser = Parser(game_conf_path)
    game_params = parser.parse()

    game = Game(game_params)
    while game.update():
        pass


if __name__ == "__main__":
    conf_path = parse_arguments()
    if conf_path is not None:
        main(conf_path)
