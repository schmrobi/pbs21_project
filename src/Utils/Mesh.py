import dmsh
import meshio
import optimesh
import numpy as np
import meshplex
from pathlib import Path
from typing import Tuple, List

PROJECT_ROOT_PATH = Path(__file__).parent.parent.parent
MESHES_DIR_PATH = PROJECT_ROOT_PATH.joinpath(Path("Meshes")).resolve()


def create_circle_mesh(
        x0: float,  # x-coordinate of circle center
        y0: float,  # y-coordinate of circle center
        r: float,  # radius of circle
        d: float,  # distance between each pair of vertices in the mesh
        oid: int  # the circle object's id
) -> None:
    """
    Create triangle mesh of a circle_ID given center and radius and d
    vertices indexed 0 ..N
    triangles triple of indices (x,y,z)
    And write to a file
    """
    create = not any([f.name == f"circle_{oid}.vtk" for f in MESHES_DIR_PATH.iterdir() if f.is_file()])
    if create:
        geo = dmsh.Circle([x0, y0], r)  # center, radius
        X, cells = dmsh.generate(geo, d)  # geo, target_edge_size ie. initial length L
        # optionally optimize the mesh
        X, cells = optimesh.optimize_points_cells(X, cells, "CVT (full)", 1.0e-10, 100)  # tol, max_nr_steps
        # and write it to a file
        output_file_path = MESHES_DIR_PATH.joinpath(Path(f"circle_{oid}.vtk").resolve())
        meshio.Mesh(X, {"triangle": cells}).write(output_file_path)


def get_mesh(oid: int, option: int) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    return the vertex coordinates and triangles of circle_ID
    """

    lines: List[Tuple[int, int]] = []
    input_file_path = MESHES_DIR_PATH.joinpath(Path(f"circle_{oid}.vtk")).resolve()
    mesh = meshio.read(input_file_path)
    vertices = np.delete(mesh.points, 2, 1)
    n_vertices = vertices.shape[0]

    links = np.full((n_vertices, n_vertices), 0)
    triangles: List[Tuple[int, int, int]] = mesh.cells_dict['triangle']

    mesh = meshplex.MeshTri(vertices, triangles)
    outer_v = np.where(mesh.is_boundary_point == True)[0]

    for t in triangles:
        # update links
        links[t[1], t[2]] = links[t[2], t[1]] = 1
        links[t[2], t[0]] = links[t[0], t[2]] = 1
        links[t[0], t[1]] = links[t[1], t[0]] = 1
        # update lines
        lines.append((t[0], t[1]))
        lines.append((t[1], t[2]))
        lines.append((t[2], t[0]))

    outer_e: List[Tuple[int, int]] = []

    # recover the outer edges:
    for t in triangles:
        x1 = t[0]
        x2 = t[1]
        x3 = t[2]

        if x1 in outer_v and x2 in outer_v:
            outer_e.append((x1, x2))
            continue

        if x1 in outer_v and x3 in outer_v:
            outer_e.append((x3, x1))
            continue

        if x3 in outer_v and x2 in outer_v:
            outer_e.append((x2, x3))
            continue

    def find(x, x1, x2):
        for t in triangles:
            if x1 in t and x2 in t and x not in t:
                for x_ in t:
                    if x_ != x1 and x_ != x2:
                        return x_
        return None

    def add(x1, x2):
        if x1 is not None:
            links[x1, x2] = links[x2, x1] = 1
            lines.append([x1, x2])

            # OPTIONALLY: EXPAND MESH

    for t in triangles:
        x1 = t[0]
        x2 = t[1]
        x3 = t[2]
        if option == 1:
            # OPTION1: single additional link for outer vertex to opposite vertex
            if x3 in outer_v and x1 not in outer_v and x2 not in outer_v:
                x0 = find(x3, x1, x2)
                add(x0, x3)
            if x2 in outer_v and x1 not in outer_v and x3 not in outer_v:
                x0 = find(x2, x1, x3)
                add(x0, x2)
            if x1 in outer_v and x3 not in outer_v and x2 not in outer_v:
                x0 = find(x1, x2, x3)
                add(x0, x1)
        elif option == 2:
            # OPTION 2: additional links on outermost layer
            if x3 in outer_v and ((x1 not in outer_v and x2 in outer_v) or (x1 in outer_v and x2 not in outer_v)):
                x0 = find(x3, x1, x2)
                add(x0, x3)
            if x2 in outer_v and ((x1 not in outer_v and x3 in outer_v) or (x1 in outer_v and x3 not in outer_v)):
                x0 = find(x2, x1, x3)
                add(x0, x2)
            if x1 in outer_v and ((x3 not in outer_v and x2 in outer_v) or (x3 in outer_v and x2 not in outer_v)):
                x0 = find(x1, x2, x3)
                add(x0, x1)
        elif option == 3:
            # OPTION 3: additional links for all triangles
            x0 = find(x3, x1, x2)
            add(x0, x3)
            x0 = find(x2, x1, x3)
            add(x0, x2)
            x0 = find(x1, x2, x3)
            add(x0, x1)

    return vertices, links, np.array(lines), outer_v, np.array(outer_e)
