import numpy as np
from src.SimulationObjects.RigidObject import RigidObject
from src.Geometry.Polygon import Polygon
from typing import List, Optional


class CollisionsHandler:
    def __init__(self, k: float, rigid_objs: List[RigidObject], method="elastic"):
        self.rigid_objs = rigid_objs
        self.method = method
        self.collision_forces: Optional[np.ndarray] = None
        self.k_collision = k

    def get_collision_forces(
            self,
            end_positions: np.ndarray,
            start_positions: np.ndarray,
            membership: np.ndarray,
            polygons: List[Polygon]
    ) -> np.ndarray:
        """
        Given the rigid bodies and soft bodies in the scene, returns the collision forces acting upon the particles with
        trajectories which start in `start_positions` and end in `end_positions`.

        Parameters:
            end_positions (np.ndarray): The end positions of the trajectories of the particles in the simulation.
            start_positions (np.ndarray): The start positions of the trajectories of the particles in the simulation.
            membership (np.ndarray): The indices of the polygons in `polygons` which each particle is a member of.
            polygons (List[Polygon]): A list of all the soft body shells in the simulation.

        Returns:
            np.ndarray: the collision forces acting on the particles in the simulation. The index of each element in
            the return numpy array corresponds to the index of the particle in the first three parameters.
        """
        N = end_positions.shape[0]
        self.collision_forces = np.zeros(shape=(N, 2), dtype=float)
        rigid_shapes: List[Polygon] = [r.get_shape() for r in self.rigid_objs]

        if self.method == "elastic":
            for i in range(N):
                for r in rigid_shapes:
                    collision_data = r.find_collided_edge(start_positions[i], end_positions[i])
                    if collision_data is not None:
                        collided_edge, penetration_depth = collision_data
                        self.collision_forces[i] += self.elastic_collision_force(penetration_depth,
                                                                                 collided_edge.normal,
                                                                                 self.k_collision)

                for j, p in enumerate(polygons):
                    if membership[i] == j:
                        continue
                    collision_data = p.find_collided_edge(start_positions[i], end_positions[i])
                    if collision_data is not None:
                        collided_edge, penetration_depth = collision_data
                        self.collision_forces[i] += self.elastic_collision_force(penetration_depth,
                                                                                 collided_edge.normal,
                                                                                 self.k_collision)

        else:
            raise NotImplemented()

        return self.collision_forces

    @staticmethod
    def elastic_collision_force(delta_r: float, normal: np.ndarray, k_collision) -> np.ndarray:
        return k_collision * delta_r * normal
