import json
from pathlib import Path
from src.Game import GameParams
from src.SimulationObjects.Obstacle import ObstacleParams
from src.SimulationObjects.Racket import RacketParams
from src.SimulationObjects.Wall import WallParams
from src.SimulationObjects.SoftBodyObjects import SoftBodyParams
from typing import Dict, Tuple, List


PROJECT_ROOT_PATH = Path(__file__).parent.parent
GAME_SETUPS_DIR_PATH = PROJECT_ROOT_PATH.joinpath(Path(f"Configurations/GameSetups")).resolve()
GAME_ARENA_DIR_PATH = PROJECT_ROOT_PATH.joinpath(Path(f"Configurations/GameArena")).resolve()
SOFT_BODY_MESHES_DIR_PATH = PROJECT_ROOT_PATH.joinpath(Path(f"Configurations/SoftBodyMeshes")).resolve()
SOFT_BODY_PHYS_DIR_PATH = PROJECT_ROOT_PATH.joinpath(Path(f"Configurations/SoftBodyPhysicalTraits")).resolve()


class Parser:

    def __init__(self, game_conf_path: Path):
        self.game_conf_path = game_conf_path

    def parse(self) -> GameParams:
        expected_keys = ["game_arena", "soft_bodies", "dt", "solver_method"]
        with self.game_conf_path.open("r") as game_conf:
            configs = json.load(game_conf)
            # check game configuration validity
            for k in expected_keys:
                if k not in configs.keys():
                    raise KeyError(
                        f"An entry called '{k}' is missing from the game "
                        f"configuration file {self.game_conf_path}."
                    )
            dt = configs["dt"]
            solver_method = configs["solver_method"]
            walls, racket, obstacles = self.parse_game_arena_conf(configs["game_arena"])
            soft_bodies = self.parse_soft_bodies_conf(configs["soft_bodies"])
            collision_resistance = configs["collision_resistance"]
            
        game_params = GameParams(dt=dt, solver_method=solver_method, walls=walls, racket=racket,
                                 obstacles=obstacles, soft_bodies=soft_bodies, k_collision=collision_resistance)

        return game_params

    @staticmethod
    def parse_game_arena_conf(arena_conf_name: str) -> Tuple[List[WallParams], RacketParams, List[ObstacleParams]]:
        expected_keys = ["racket", "walls", "obstacles"]
        conf_path = GAME_ARENA_DIR_PATH.joinpath(f"{arena_conf_name}.json").resolve()
        if not conf_path.exists():
            raise RuntimeError("Game arena configuration file does not exist!")

        with conf_path.open("r") as conf:
            configs = json.load(conf)
            # check configuration file validity
            for k in expected_keys:
                if k not in configs.keys():
                    raise KeyError(
                        f"An entry called '{k}' is missing from the game "
                        f"arena configuration file {conf_path}."
                    )
            # get racket params
            racket = RacketParams(
                center=configs["racket"]["init_pos"],
                width=configs["racket"]["width"],
                height=configs["racket"]["height"]
            )
            # get walls params
            walls = list()
            for w in configs["walls"]:
                walls.append(WallParams(
                    left=w["left"],
                    right=w["right"],
                    bottom=w["bottom"],
                    top=w["top"]
                ))
            # get obstacles params
            obstacles = list()
            for o in configs["obstacles"]:
                obstacles.append(ObstacleParams(vertices=o["vertices"], clockwise=o["clockwise"]))

        return walls, racket, obstacles

    @staticmethod
    def parse_soft_bodies_conf(soft_bodies_configs: List[Dict[str, str]]) -> List[SoftBodyParams]:
        soft_bodies = list()
        for soft_body_configs in soft_bodies_configs:
            expected_keys = ["mesh", "physical_traits"]
            for k in expected_keys:
                if k not in soft_body_configs.keys():
                    raise KeyError(
                        f"An entry called '{k}' is missing from the game "
                        f"setup configuration file."
                    )
            soft_bodies.append(
                Parser.parse_soft_body_conf(soft_body_configs["mesh"], soft_body_configs["physical_traits"])
            )
        return soft_bodies

    @staticmethod
    def parse_soft_body_conf(mesh_conf_name: str, phys_conf_name: str) -> SoftBodyParams:

        mesh_conf_path = SOFT_BODY_MESHES_DIR_PATH.joinpath(f"{mesh_conf_name}.json")
        with mesh_conf_path.open("r") as conf:
            mesh_configs = json.load(conf)

        phys_conf_path = SOFT_BODY_PHYS_DIR_PATH.joinpath(f"{phys_conf_name}.json")
        with phys_conf_path.open("r") as conf:
            phys_configs = json.load(conf)

        return SoftBodyParams(
            oid=mesh_configs["oid"],
            k=phys_configs["stiffness"],
            mass=phys_configs["total_mass"],
            gamma=phys_configs["dampening"],
            init_pos=mesh_configs["init_position"],
            init_spring_len=mesh_configs["edge_length"],
            density_type=mesh_configs["density_type"],
            radius=mesh_configs["radius"]
        )
