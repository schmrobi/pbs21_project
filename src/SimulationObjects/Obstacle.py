import numpy as np
from src.SimulationObjects.RigidObject import RigidObject
from src.Geometry.Polygon import Polygon
from dataclasses import dataclass
from typing import Tuple, List


@dataclass()
class ObstacleParams:
    vertices: List[Tuple[float, float]]
    clockwise: bool


class Obstacle(RigidObject):

    def __init__(self, params: ObstacleParams):
        self.vertices = [np.array(v) for v in params.vertices]
        self.clockwise = params.clockwise
        polygon = Polygon(self.vertices, params.clockwise)
        RigidObject.__init__(self, shape=polygon)
 
    def display(self, gui):
        """
        Renders the Obstacle as a polygon.
        """
        n = len(self.vertices)
        edges = [[self.vertices[i], self.vertices[(i + 1) % n]] for i in range(n)]
        edges = np.array(edges)
        gui.lines(edges[:, 0], edges[:, 1], color=0xffffff)
