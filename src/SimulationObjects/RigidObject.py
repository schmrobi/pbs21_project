from src.SimulationObjects.SimulationObject import SimulationObject
from src.Geometry.Polygon import Polygon


class RigidObject(SimulationObject):

    def __init__(self, shape: Polygon) -> None:
        SimulationObject.__init__(self)
        self.shape = shape

    def get_shape(self) -> Polygon:
        return self.shape

    def set_shape(self, shape: Polygon) -> None:
        self.shape = shape
