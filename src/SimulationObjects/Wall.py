import taichi as ti
from src.SimulationObjects.RigidObject import RigidObject
from src.Geometry.Rectangle import Rect
from dataclasses import dataclass


@dataclass()
class WallParams:
    left: float
    right: float
    bottom: float
    top: float


class Wall(RigidObject):

    def __init__(self, params: WallParams):
        rect = Rect(params.left, params.right, params.bottom, params.top)
        RigidObject.__init__(self, shape=rect)
        self.rect = rect

    def display(self, gui):
        bl_corner = ti.Vector([self.rect.left, self.rect.bottom])
        tr_corner = ti.Vector([self.rect.right, self.rect.top])
        gui.rect(bl_corner, tr_corner, color=0xffffff)
