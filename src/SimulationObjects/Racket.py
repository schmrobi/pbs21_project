import taichi as ti
from src.SimulationObjects.RigidObject import RigidObject
from src.Geometry.Rectangle import Rect
from dataclasses import dataclass
from typing import Tuple


@dataclass()
class RacketParams:
    center: Tuple[float, float]
    width: float
    height: float


class Racket(RigidObject):

    def __init__(self, params: RacketParams):
        rect = Rect.from_center_and_dims(params.center[0], params.center[1], params.width, params.height)
        RigidObject.__init__(self, shape=rect)
        self.center = params.center
        self.width = params.width
        self.height = params.height
        self.lateral_velocity = 0.0

    def step(self):
        self.center[0] = self.center[0] + self.lateral_velocity
        # Clamp racket position into boundaries
        self.center[0] = max(0.1 + 0.5 * self.width, self.center[0])
        self.center[0] = min(0.9 - 0.5 * self.width, self.center[0])
        self.set_shape(
            Rect.from_center_and_dims(self.center[0], self.center[1], self.width, self.height)
        )

    # Renders the racket as a rectangle
    def display(self, gui):
        bl_corner = ti.Vector([self.center[0] - 0.5 * self.width, self.center[1] - 0.5 * self.height])
        tr_corner = ti.Vector([self.center[0] + 0.5 * self.width, self.center[1] + 0.5 * self.height])
        gui.rect(bl_corner, tr_corner, color=0xffffff)
    
    # processes the player input and sets the racked speed on the x axis accordingly
    def process_input(self, gui):
        gui.get_event()
        if gui.is_pressed('a', ti.GUI.LEFT):
            self.lateral_velocity = -0.01
        elif gui.is_pressed('d', ti.GUI.RIGHT):
            self.lateral_velocity = 0.01
        else:
            self.lateral_velocity = 0.0
