from src.SimulationObjects.SimulationObject import SimulationObject


class Text(SimulationObject):

    def __init__(self, message: str, x_position: float, y_position: float, font_size: int, color: int):
        SimulationObject.__init__(self)
        self.message = message
        self.position = (x_position, y_position)
        self.font_size = font_size
        self.color = color

    def display(self, gui):
        gui.text(self.message, self.position, self.font_size, self.color)
