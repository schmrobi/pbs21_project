import taichi as ti


@ti.data_oriented
class SimulationObject:
    """
    This is the superclass of objects that should be simulated in the scene.
    """
    def __init__(self) -> None:
        pass

    # Note: the execution order of the following three methods goes as follows: process_input -> step -> display.
    # The advantage of processing the input before the step is that the player input can be directly processed in
    # the same frame, effectively reducing input latency.
    # The same goes for rendering directly after the simulation update step.

    def step(self) -> None:
        """
        This method is called for each physics step.
        """
        pass

    def display(self, gui) -> None:
        """
        This method is called when the object is rendered.
        """
        pass

    def process_input(self, gui) -> None:
        """
        this method is called on each frame and is intended for input processing.
        """
        pass
