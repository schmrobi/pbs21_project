import taichi as ti
import numpy as np
from src.SimulationObjects.SimulationObject import SimulationObject
from src.Utils import Mesh
from src.Utils.Collision import CollisionsHandler
from src.Geometry.Polygon import Polygon
from scipy.linalg import cho_factor, LinAlgError
from scipy.sparse.linalg import gmres
from dataclasses import dataclass
from typing import Optional, List, Tuple


@dataclass()
class SoftBodyParams:
    oid: int
    k: float
    mass: float
    gamma: float
    init_pos: Tuple[float, float]
    radius: float
    init_spring_len: float
    density_type: int


@ti.data_oriented
class SoftBodyObjects(SimulationObject):
    def __init__(
            self,
            dt: float,
            solver: str,
            collisions_handler: CollisionsHandler,
            soft_bodies: List[SoftBodyParams],
    ):
        SimulationObject.__init__(self)
        self.dt = dt
        self.solver_method = solver
        self.collisions_handler = collisions_handler
        # global physical constants
        self.ks: List[float] = [s.k for s in soft_bodies]
        self.total_mass: List[float] = [s.mass for s in soft_bodies]
        self.gammas: List[float] = [s.gamma for s in soft_bodies]
        self.gravity_acc = -9.82
        self.init_pos: List[Tuple[float, float]] = [s.init_pos for s in soft_bodies]
        self.radius: List[float] = [s.radius for s in soft_bodies]
        self.init_spring_len: List[float] = [s.init_spring_len for s in soft_bodies]
        self.mesh_option: List[int] = [s.density_type for s in soft_bodies]
        self.oids: List[int] = [s.oid for s in soft_bodies]
        self.n_objs = len(soft_bodies)

        # mesh
        self.N_all: Optional[int] = 0
        self.Ns: List[int] = list()
        self.vertices_list: List[np.ndarray] = list()
        self.links_list: List[np.ndarray] = list()
        self.links: Optional[ti.field] = None
        self.lines_list: List[np.ndarray] = list()
        self.outer_vertices: List[np.ndarray] = list()
        self.inner_vertices: List[np.ndarray] = list()
        self.outer_edges: List[np.ndarray] = list()
        self.membership: Optional[np.ndarray] = None
        self.m_inv: Optional[ti.field] = None
        self.m: Optional[ti.field] = None
        self.M: Optional[ti.field] = None
        self.k: Optional[ti.field] = None
        self.gamma: Optional[ti.field] = None
        self.init_mesh()

        # local mesh constants
        self.gravity_force = ti.Vector.field(2, float, self.N_all)
        self.init_gravity_force()
        
        # variables
        self.x = ti.Vector.field(2, float, self.N_all)
        self.x_curr = ti.Vector.field(2, float, self.N_all)
        self.v = ti.Vector.field(2, float, self.N_all)
        self.dx = ti.Vector.field(2, float, self.N_all)
        self.Jg = ti.Matrix.field(2, 2, float, (self.N_all, self.N_all))
        self.g = ti.Vector.field(2, float, self.N_all)

        self.f = ti.Vector.field(2, float, self.N_all)
        self.f_internal = ti.Vector.field(2, float, self.N_all)
        self.f_collisions = ti.Vector.field(2, float, self.N_all)
        self.g = ti.Vector.field(2, float, self.N_all)
        self.Jx = ti.Matrix.field(2, 2, float, (self.N_all, self.N_all))
        self.Jv = ti.Matrix.field(2, 2, float, (self.N_all, self.N_all))
        self.L0 = ti.field(float, shape=(self.N_all, self.N_all))
        # self.L0.from_numpy(np.zeros(shape=(self.N_all, self.N_all)))
        self.init_state_variables()
        self.init_initial_lengths()
        self.init_Jv()

    def get_global_index_range(self, obj_idx: int) -> Tuple[int, int, int]:
        """
        Returns the total number of vertices in the object, and their global start and end indices.
        """
        N = self.Ns[obj_idx]
        start = self.local_to_global(obj_idx, 0)
        end = start + N
        return N, start, end

    def local_to_global(self, obj_idx: int, local_idx: int) -> int:
        """
        Maps an "in-object" index, i.e. from 0 to the number of vertices in that object, to the global system range.
        """
        offset = sum([self.Ns[i] for i in range(obj_idx)], start=0)
        return local_idx + offset

    def global_to_local(self, obj_idx: int, global_idx: int) -> int:
        """
        Maps a global index, i.e. from 0 to the total number of vertices in the system, to the object's local
        indices range, i.e. from 0 to the number of vertices in that object.
        """
        offset = sum([self.Ns[i] for i in range(obj_idx)], start=0)
        return global_idx - offset

    def init_mesh(self) -> None:
        if len(self.oids) < 1:
            raise RuntimeError()

        for i in range(self.n_objs):
            SoftBodyObjects.create_mesh(self.oids[i], self.init_pos[i], self.radius[i], self.init_spring_len[i])
            verts, links, lines, outer_verts, outer_edges = Mesh.get_mesh(self.oids[i], self.mesh_option[i])

            N_i = len(verts)
            self.N_all += N_i
            self.Ns.append(N_i)
            self.vertices_list.append(verts)
            self.links_list.append(links)
            self.outer_vertices.append(outer_verts)
            inner_verts = np.array([i for i in range(verts.shape[0]) if i not in outer_verts])
            self.inner_vertices.append(inner_verts)
            self.outer_edges.append(outer_edges)
            self.lines_list.append(lines)

        self.links = ti.field(int, shape=(self.N_all, self.N_all))
        self.membership = np.full(shape=self.N_all, fill_value=0)
        self.k = ti.field(float, self.N_all)
        self.gamma = ti.field(float, self.N_all)
        self.m = ti.field(float, self.N_all)
        self.m_inv = ti.field(float, self.N_all)
        self.M = ti.Matrix.field(2, 2, float, self.N_all)

        for i in range(self.n_objs):
            N, start, end = self.get_global_index_range(i)
            obj_links = self.links_list[i]
            for j in range(start, end):
                self.membership[j] = i
                self.k[j] = self.ks[i]
                self.gamma[j] = self.gammas[i]
                m = self.total_mass[i] / self.Ns[i]
                m_inv = 1.0 / m
                self.m[j] = m
                self.m_inv[j] = m_inv
                self.M[j] = ti.Matrix([
                    [m, 0.0],
                    [0.0, m]
                ])
                for k in range(start, end):
                    local_j = self.global_to_local(i, j)
                    local_k = self.global_to_local(i, k)
                    self.links[j, k] = int(obj_links[local_j, local_k])

    @staticmethod
    def create_mesh(oid: int, position: Tuple[float, float], radius: float, distance: float) -> None:
        Mesh.create_circle_mesh(x0=position[0], y0=position[1], r=radius, d=distance, oid=oid)

    def init_gravity_force(self) -> None:
        for i in ti.ndrange(self.N_all):
            self.gravity_force[i].y = self.m[i] * self.gravity_acc
          
    def init_state_variables(self):
        for i in range(self.n_objs):
            verts = self.vertices_list[i]
            _, start, end = self.get_global_index_range(i)
            for j in range(start, end):
                local_j = self.global_to_local(i, j)
                self.x[j] = ti.Vector(verts[local_j])
                self.v[j] = ti.Vector([0.0, 0.0])

    @ti.kernel
    def init_initial_lengths(self):
        for i, j in ti.ndrange(self.N_all, self.N_all):
            if self.links[i, j] != 0:
                self.L0[i, j] = (self.x[j] - self.x[i]).norm()

    @ti.kernel
    def init_Jv(self):
        """
        Initializes the constant df/dv term in the jacobian of g (for the implicit solver).
        """
        for i, j in ti.ndrange(self.N_all, self.N_all):
            viscous_Jv = ti.Matrix([
                [self.gamma[i], 0],
                [0, self.gamma[i]]
            ])
            if self.links[i, j] != 0:
                self.Jv[i, j] += viscous_Jv
            if i == j:
                self.Jv[i, i] -= viscous_Jv

    def step(self):
        if self.solver_method == "explicit":
            self.explicit_euler_step()
        elif self.solver_method == "implicit":
            self.implicit_euler_step()
        else:
            raise NotImplemented()

    def implicit_euler_step(self):
        self.x_curr = self.x  # save current positions of particles
        T = 1
        while T > 0:
            self.comp_f()
            self.reset_Jx()
            self.comp_Jx()
            self.solve_newton_method_step()
            self.update_x()
            T -= 1

        self.comp_f()
        self.euler_v_advance()

    @ti.kernel
    def update_x(self):
        for i in ti.ndrange(self.N_all):
            self.x[i] += self.dx[i]

    @ti.kernel
    def euler_v_advance(self):
        for i in ti.ndrange(self.N_all):
            self.v[i] += self.dt * self.m_inv[i] * self.f[i]

    def explicit_euler_step(self):
        self.explicit_euler_x_advance()
        self.comp_f()
        self.euler_v_advance()
        
    @ti.kernel
    def explicit_euler_x_advance(self):
        for i in ti.ndrange(self.N_all):
            self.x_curr[i] = self.x[i]
            self.x[i] += self.dt * self.v[i]

    @staticmethod
    def regularize_Jg(Jg):
        """
        Eliminates the negative eigenvalues in J[g] by iteratively adding an increasing value to the diagonal.
        This is done in order to deal with the indefiniteness of J[g], in order to be able to
        solve the equation J[g] * dx = g.
        """
        alpha0 = 1e-4
        it = 0
        while True:
            try:
                cho_factor(Jg)
            except LinAlgError:
                reg = alpha0 * (2.0 ** it)
                for i in range(Jg.shape[0]):
                    Jg[i, i] += reg
                it += 1
            else:
                break
        return Jg

    @ti.kernel
    def comp_Jg(self):
        """
        Computes the jacobian of g, J[g], described in Newton's method
        """
        for i, j in ti.ndrange(self.N_all, self.N_all):
            self.Jg[i, j] = self.M[i] - self.dt * self.Jv[i, j] - (self.dt ** 2) * self.Jx[i, j]

    @ti.kernel
    def comp_g(self):
        """
        Computes the function g described in Newton's method
        """
        for i in ti.ndrange(self.N_all):
            self.g[i] = -(self.m[i] * self.dx[i] - (self.dt ** 2) * self.f[i] - self.dt * self.m[i] * self.v[i])

    def solve_newton_method_step(self) -> None:
        """
        Solves the equation J[g] * dx = g for dx (as described in Newton's method), and returns dx.
        """
        self.comp_Jg()
        self.comp_g()
        Jg = self.Jg.to_numpy().reshape(2 * self.N_all, 2 * self.N_all)
        g = self.g.to_numpy().reshape(2 * self.N_all)
        Jg = self.regularize_Jg(Jg)
        dx, _ = gmres(Jg, g)
        self.dx.from_numpy(dx.reshape(self.N_all, 2))

    def comp_f(self):
        """
        Computes the forces acting on all the particles from internal and external sources.
        """
        self.reset_f()
        self.comp_f_internal()
        self.comp_f_collisions()
        self.fill_f()

    @ti.kernel
    def reset_f(self):
        for i in ti.ndrange(self.N_all):
            self.f[i] *= 0
            self.f_internal[i] *= 0
            self.f_collisions[i] *= 0

    @ti.kernel
    def comp_f_internal(self):
        """
        Computes the forces acting on all the particles from internal sources (spring forces).
        """
        for i, j in ti.ndrange(self.N_all, self.N_all):
            if i < j and self.links[i, j] != 0:
                # computed internal spring force
                # points to the jth particle
                disp = self.x[j] - self.x[i]
                L0 = self.L0[i, j]
                # internal spring force
                f = self.k[i] * (disp.norm() - L0) * disp.normalized()
                self.f_internal[i] += f
                self.f_internal[j] -= f

    def comp_f_collisions(self):
        """
        Computes the forces acting on all the particles from external sources as a result of collisions.
        """
        soft_body_shells = self.get_shells()
        f_collisions = self.collisions_handler.get_collision_forces(
            self.x.to_numpy(),
            self.x_curr.to_numpy(),
            self.membership,
            soft_body_shells
        )
        self.f_collisions.from_numpy(f_collisions)

    @ti.kernel
    def fill_f(self):
        for i in ti.ndrange(self.N_all):
            self.f[i] = self.f_internal[i] + self.f_collisions[i] + self.gravity_force[i] - self.v[i] * self.gamma[i]

    @ti.kernel
    def reset_Jx(self):
        for i, j in ti.ndrange(self.N_all, self.N_all):
            self.Jx[i, j] *= 0.0

    @ti.kernel
    def comp_Jx(self):
        """
        Compute the df/dx term in the jacobian of g.
        """
        for i in ti.ndrange(self.N_all):
            self.comp_elastic_Jx(i)

    @ti.func
    def comp_elastic_Jx(self, i: ti.i32) -> ti.Matrix:
        """
        Computes the block in the df/dx term in the jacobian of g representing the elastic internal
        forces for the ith vertex.
        """
        for j in ti.ndrange(i):
            if self.links[i, j] != 0:
                Jx_ij = self.comp_spring_Jx(i, j)
                self.Jx[i, j] += Jx_ij
                self.Jx[j, i] += Jx_ij
                self.Jx[i, i] -= Jx_ij
                self.Jx[j, j] -= Jx_ij

    @ti.func
    def comp_spring_Jx(self, i: int, j: int) -> ti.Matrix:
        """
        Computes a single term in the jacobian of g, representing the spring force jacobian between
        the ith and jth vertices.
        """
        unit = ti.Matrix([[1.0, 0.0], [0.0, 1.0]])
        disp = self.x[j] - self.x[i]
        kL0 = self.k[i] * self.L0[i, j]
        block = self.k[i] * unit
        block -= (kL0 / disp.norm()) * unit
        block += (kL0 / disp.norm() ** 3) * disp.outer_product(disp)
        return block

    def display(self, gui):
        pos = self.x.to_numpy()
        for i in range(self.n_objs):
            # display inner particles
            inner_verts = [self.local_to_global(i, v) for v in self.inner_vertices[i]]
            gui.circles(pos[inner_verts], radius=2.5, color=0xFFFFFF)
            # display outer particles
            outer_verts = [self.local_to_global(i, v) for v in self.outer_vertices[i]]
            gui.circles(pos[outer_verts], radius=2.5, color=0xFF0000)
            # display springs between particles
            lines = self.lines_list[i]
            start_indices = [self.local_to_global(i, v) for v in lines[:, 0]]
            end_indices = [self.local_to_global(i, v) for v in lines[:, 1]]
            gui.lines(pos[start_indices], pos[end_indices], 0.8, color=0xFFFFFF)

    def get_y_center_of_mass(self, N, start, end) -> Tuple[float, float]:
        y_acc = 0
        for i in range(start, end):
            y_acc += self.x[i][1]
        return y_acc / N
    
    def get_y_centers_of_masses(self):
        coms = []
        for i in range(self.n_objs):
            N, start, end = self.get_global_index_range(i)
            com = self.get_y_center_of_mass(N, start, end)
            coms.append(com)
        return np.array(coms)	
    
    def get_shells(self) -> List[Polygon]:
        polygons = list()
        x_np = self.x.to_numpy()
        for i in range(self.n_objs):
            outer_verts = [self.local_to_global(i, v) for v in self.outer_vertices[i]]
            polygon = Polygon(x_np[outer_verts], clockwise=False)
            polygons.append(polygon)
        return polygons
