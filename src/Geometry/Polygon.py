import numpy as np
from src.Geometry.PolygonEdge import PolygonEdge
from src.Geometry.LineSegment2D import LineSegment2D, line_segments_intersection, line_segments_intersect
from src.Geometry.Constants import epsilon
from typing import List, Optional, Tuple


class Polygon:
    """
    A class representing a polygon. When initializing the polygon, the vertices that are given need to be
    in the `clockwise` order (clockwise or counterclockwise) to make sure the normals are calculated correctly.
    """
    def __init__(self, vertices: List[np.ndarray], clockwise: bool) -> None:
        if len(vertices) < 3:
            raise RuntimeError("A polygon can't have less than 3 vertices!")
        self.clockwise = clockwise
        self.edges: List[PolygonEdge] = list()
        self.init_edges(vertices)

    def init_edges(self, vertices: List[np.ndarray]) -> None:
        n = len(vertices)
        for i in range(n):
            p1 = vertices[i]
            p2 = vertices[(i + 1) % n]
            v = p2 - p1
            if self.clockwise:
                edge_normal = np.array([-v[1], v[0]])
            else:
                edge_normal = np.array([v[1], -v[0]])

            edge_normal = edge_normal / np.linalg.norm(edge_normal)
            self.edges.append(PolygonEdge(p1, p2, edge_normal))

    def get_edges(self) -> List[PolygonEdge]:
        return self.edges

    def closest_edge(self, point: np.ndarray) -> Tuple[PolygonEdge, float]:
        """
        Returns the closest edge to `point` and the distance to that edge.
        """
        dists = [e.dist(point) for e in self.edges]
        closest_edge_idx = np.argmin(dists)
        return self.edges[closest_edge_idx], dists[closest_edge_idx]

    def contains(self, point: np.ndarray) -> bool:
        """
        Tests if `point` is contained within the polygon, by counting the number of time a ray shooting out from
        `point` to infinity intersects the polygon's edges. If there are an odd number of intersection, `point`
        is contained within the polygon, else it is not.
        """
        point_at_infinity = point + np.array([0.0, 100.0])
        ray = LineSegment2D(point, point_at_infinity)
        counter = 0
        for e in self.edges:
            if line_segments_intersect(e, ray):
                counter += 1

        return (counter % 2) == 1

    def find_collided_edge(self, start: np.ndarray, end: np.ndarray) -> Optional[Tuple[PolygonEdge, float]]:
        """
        Determines if a resting collision ot an active collision takes place with a point object with
        starting position `start` and final position `end`.
        If no edge is collided, returns None.
        Else, returns the collided edge and the depth of the penetration (along the edge's normal).
        """
        if np.linalg.norm(end - start) < epsilon:
            return self.resting_collision(end)
        else:
            return self.dynamic_collision(start, end)

    def resting_collision(self, point: np.ndarray) -> Optional[Tuple[PolygonEdge, float]]:
        """
        Determines if `point` is inside the polygon.
        If it is, returns the closest edge to `point` and the distance to that edge.
        Else, returns None.
        """
        if not self.contains(point):
            return None

        edge, dist_to_edge = self.closest_edge(point)
        return edge, dist_to_edge

    def dynamic_collision(self, start: np.ndarray, end: np.ndarray) -> Optional[Tuple[PolygonEdge, float]]:
        """
        Determines if the point object collides with and penetrates the polygon, assuming a linear trajectory
        starting at `start` and ends `end`.
        If it does, returns the edge which the point object collided with and the
        depth (along that edge's normal) of penetration.
        Else, returns None.
        Raises RuntimeError if the trajectory is illegal (starts inside the polygon or collides with multiple edges).
        """
        if self.contains(start):
            if self.contains(end):
                raise RuntimeError("Trajectory is fully contained inside the polygon!")
            else:
                raise RuntimeError("Particle started the trajectory in the polygon and finished outside of it!")

        trajectory = LineSegment2D(start, end)
        intersected_edges: List[PolygonEdge] = list()
        intersections: List[np.ndarray] = list()
        for e in self.edges:
            intersection = line_segments_intersection(e, trajectory)
            if intersection is not None:
                intersected_edges.append(e)
                intersections.append(intersection)

        if len(intersected_edges) > 1:
            raise RuntimeError("Trajectory penetrates the polygon more than once!")

        if len(intersected_edges) != 1:
            return None

        collided_edge = intersected_edges[0]
        collision_point = intersections[0]
        penetration_depth = np.linalg.norm((collision_point - end) * collided_edge.normal)
        return collided_edge, penetration_depth
