import numpy as np
from src.Geometry.Polygon import Polygon
from src.Geometry.Constants import epsilon
from typing import List


class Rect(Polygon):

    def __init__(self, left: float, right: float, bottom: float, top: float):
        self.left = left
        self.right = right
        self.bottom = bottom
        self.top = top
        vertices: List[np.ndarray] = list()
        vertices.append(np.array([self.left, self.top]))  # top-left vertex
        vertices.append(np.array([self.right, self.top]))  # top-right vertex
        vertices.append(np.array([self.right, self.bottom]))  # bottom-right vertex
        vertices.append(np.array([self.left, self.bottom]))  # bottom-left vertex
        Polygon.__init__(self, vertices=vertices, clockwise=True)

    @staticmethod
    def from_center_and_dims(center_x: float, center_y: float, width: float, height: float):
        return Rect(
            center_x - 0.5 * width,
            center_x + 0.5 * width,
            center_y - 0.5 * height,
            center_y + 0.5 * height
        )

    def contains(self, point: np.ndarray) -> bool:
        in_x_range = self.left - epsilon <= point[0] <= self.right + epsilon
        in_y_range = self.bottom - epsilon <= point[1] <= self.top + epsilon
        return in_x_range and in_y_range
