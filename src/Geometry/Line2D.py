import numpy as np
from src.Geometry.Constants import epsilon
from typing import Optional


class Line2D:
    """
    A representation of a two-dimensional infinite line in the form Ax + By + C = 0
    """
    def __init__(self, p1: np.ndarray, p2: np.ndarray):
        self.valid = True
        self.A: float = p2[1] - p1[1]
        self.B: float = p1[0] - p2[0]
        self.C = None
        horizontal = abs(self.A) < epsilon
        vertical = abs(self.B) < epsilon
        inv_norm = 0.0
        if horizontal and vertical:
            self.valid = False
        elif horizontal and not vertical:
            # p1[1] == p2[1], p1[0] != p2[0]
            self.C: float = p2[1] * (p2[0] - p1[0])
            inv_norm = 1.0 / abs(self.B)
        elif not horizontal and vertical:
            # p1[1] != p2[1], p1[0] == p2[0]
            self.C: float = p2[0] * (p1[1] - p2[1])
            inv_norm = 1.0 / abs(self.A)
        else:
            self.C: float = p1[1] * p2[0] - p1[0] * p2[1]
            inv_norm = 1.0 / np.sqrt(self.A * self.A + self.B * self.B)

        if self.valid:
            self.A = inv_norm * self.A
            self.B = inv_norm * self.B
            self.C = inv_norm * self.C

    def horizontal(self) -> bool:
        return np.abs(self.A) < epsilon

    def vertical(self) -> bool:
        return np.abs(self.B) < epsilon

    def at_x(self, x: float) -> Optional[float]:
        """
        If the line is not vertical, return the value `y` which solves the line equation given `x`.
        Else, returns None.
        """
        if self.vertical():
            return None
        y = (-self.A * x - self.C) / self.B
        return y

    def at_y(self, y: float) -> Optional[float]:
        """
        If the line is not horizontal, return the value `x` which solves the line equation given `y`.
        Else, returns None.
        """
        if self.horizontal():
            return None
        x = (-self.B * y - self.C) / self.A
        return x

    def distToLine(self, point: np.ndarray) -> Optional[float]:
        """
        If the line is valid, returns the distance between `point` and the line.
        Else, returns None.
        """
        if not self.valid:
            return None

        x = point[0]
        y = point[1]
        return np.abs(self.A * x + self.B * y + self.C)


def lines_parallel(l1: Line2D, l2: Line2D) -> bool:
    return abs(l2.B * l1.A - l1.B * l2.A) < epsilon


def lines_overlap(l1: Line2D, l2: Line2D) -> bool:
    return abs(l1.C - l2.C) < epsilon


def lines_intersect(l1: Line2D, l2: Line2D) -> bool:
    return l1.valid and l2.valid and (not lines_parallel(l1, l2) or lines_overlap(l1, l2))


def lines_intersection(l1: Line2D, l2: Line2D) -> Optional[np.ndarray]:
    """
    If the lines `l1` and `l2` intersect, returns the point of intersection.
    Else, returns None.
    """
    intersection = None
    if l1.valid and l2.valid:
        denom = l2.B * l1.A - l1.B * l2.A
        if np.fabs(denom) > epsilon:
            numer = l1.B * l2.C - l2.B * l1.C
            x = numer / denom
            if l1.vertical():
                y = l2.at_x(x)
            else:
                y = l1.at_x(x)
            if y is not None:
                intersection = np.array([x, y])

    return intersection
