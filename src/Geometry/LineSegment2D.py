import numpy as np
from src.Geometry.Line2D import Line2D, lines_intersection
from src.Geometry.Constants import epsilon
from typing import Optional


class LineSegment2D(Line2D):
    """
    A representation of a two-dimensional line segment in the form Ax + By + C = 0, with two end-points.
    """
    def __init__(self, p1: np.ndarray, p2: np.ndarray):
        Line2D.__init__(self, p1=p1, p2=p2)
        self.p1 = p1
        self.p2 = p2
        self.x_min = np.fmin(p1[0], p2[0])
        self.x_max = np.fmax(p1[0], p2[0])
        self.y_min = np.fmin(p1[1], p2[1])
        self.y_max = np.fmax(p1[1], p2[1])

    def in_x_range(self, x: float) -> bool:
        return self.x_min - epsilon <= x <= self.x_max + epsilon

    def in_y_range(self, y: float) -> bool:
        return self.y_min - epsilon <= y <= self.y_max + epsilon

    def pointOnPerpendicularPlane(self, point: np.ndarray) -> bool:
        if not self.valid:
            return False
        in_p1_half_plane = np.dot(point - self.p1, self.p2 - self.p1) > -epsilon
        in_p2_half_plane = np.dot(point - self.p2, self.p2 - self.p1) < epsilon
        return in_p1_half_plane and in_p2_half_plane

    def distToLineSegment(self, point: np.ndarray) -> float:
        if self.pointOnPerpendicularPlane(point):
            return self.distToLine(point)

        dist_to_p1 = np.linalg.norm(point - self.p1)
        dist_to_p2 = np.linalg.norm(point - self.p2)
        return min(dist_to_p1, dist_to_p2)


def line_segments_intersection(ls1: LineSegment2D, ls2: LineSegment2D) -> Optional[np.ndarray]:
    """
    If the line segments `ls1` and `ls2` intersect, returns the point of intersection.
    Else, returns None.
    """
    intersection = lines_intersection(ls1, ls2)
    if intersection is not None:
        x_in_range = ls1.in_x_range(intersection[0]) and ls2.in_x_range(intersection[0])
        y_in_range = ls1.in_y_range(intersection[1]) and ls2.in_y_range(intersection[1])
        if not x_in_range or not y_in_range:
            intersection = None

    return intersection


def line_segments_intersect(ls1: LineSegment2D, ls2: LineSegment2D) -> bool:
    """
    Return true if the line segments `ls1` and `ls2` intersect, false otherwise.
    """
    intersection = line_segments_intersection(ls1, ls2)
    return intersection is not None
