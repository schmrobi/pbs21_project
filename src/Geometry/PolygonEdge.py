import numpy as np
from src.Geometry.LineSegment2D import LineSegment2D


class PolygonEdge(LineSegment2D):

    def __init__(self, p1: np.ndarray, p2: np.ndarray, normal: np.ndarray):
        LineSegment2D.__init__(self, p1=p1, p2=p2)
        self.normal = normal

    def dist(self, point: np.ndarray) -> float:
        return self.distToLineSegment(point)
