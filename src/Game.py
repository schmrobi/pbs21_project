import taichi as ti
from src.SimulationObjects.Obstacle import ObstacleParams, Obstacle
from src.SimulationObjects.RigidObject import RigidObject
from src.SimulationObjects.SimulationObject import SimulationObject
from src.SimulationObjects.Racket import Racket, RacketParams
from src.SimulationObjects.Wall import Wall, WallParams
from src.SimulationObjects.Text import Text
from src.SimulationObjects.SoftBodyObjects import SoftBodyObjects, SoftBodyParams
from src.Utils.Collision import CollisionsHandler
from dataclasses import dataclass
from typing import List


ti.init(arch=ti.gpu)


@dataclass()
class GameParams:
    dt: float
    solver_method: str
    walls: List[WallParams]
    racket: RacketParams
    obstacles: List[ObstacleParams]
    soft_bodies: List[SoftBodyParams]
    k_collision: float


class Game:
    """
    Singleton class that handles the overall game state.
    """
    def __init__(self, params: GameParams) -> None:
        self.gui = ti.GUI("Game", res=512, fast_gui=False)
        self.simulation_objects: List[SimulationObject] = list()
        # start by creating the rigid objects in the game
        rigid_objects = self.create_rigid_objs(params.walls, params.racket, params.obstacles)
        self.simulation_objects.extend(rigid_objects)
        # create the collisions handler which receives as input the rigid objects
        self.collisions_handler = CollisionsHandler(k=params.k_collision, rigid_objs=rigid_objects)
        # create the soft bodies handler
        self.simulation_objects.append(
            SoftBodyObjects(dt=params.dt, solver=params.solver_method, collisions_handler=self.collisions_handler,
                            soft_bodies=params.soft_bodies)
        )
        self.frame = 0  # for debugging
        self.game_state = "game"

    @staticmethod
    def create_rigid_objs(
            walls_params: List[WallParams],
            racket_params: RacketParams,
            obstacles: List[ObstacleParams]
    ) -> List[RigidObject]:
        rigid_objs = list()
        # create walls
        for w in walls_params:
            rigid_objs.append(Wall(w))
        # create generic obstacles
        for o in obstacles:
            rigid_objs.append(Obstacle(o))
        # create racket
        rigid_objs.append(Racket(racket_params))
        return rigid_objs

    def update_input(self):
        """
        processes the input on each object of the simulation.
        """
        for obj in self.simulation_objects:
            obj.process_input(self.gui)

    def update_physics(self) -> None:
        """
        processes the simulation aspect of each object.
        """
        for b in self.simulation_objects:
            b.step()

    def update_visuals(self) -> None:
        """
        renders the objects in the scene.
        """
        for obj in self.simulation_objects:
            obj.display(self.gui)

        self.gui.show()

    def update(self) -> bool:
        """
        runs a single update loop of the game. returns true if the game is still running.
        """
        self.frame += 1
        # update game based on state that its in
        if self.game_state == "game":
            self.update_input()
            self.update_physics()
            self.update_visuals()
        elif self.game_state == "over":
            self.update_visuals()
        else:
            raise NotImplemented

        # update game state
        if self.game_state == "game":
            if (self.simulation_objects[-1].get_y_centers_of_masses() <= 0.).any():
                self.game_state = "over"
                self.simulation_objects.append(Text("GAME OVER", 0.2, 0.7, 64, 0xff0000))
        elif self.game_state == "over":
            pass
        else:
            raise NotImplemented

        return self.gui.running
